import {
  Link,
  SectionHeader,
  SimpleTable,
  StatusLabel,
} from '@kinvolk/headlamp-plugin/lib/CommonComponents';
import { KubeObject } from '@kinvolk/headlamp-plugin/lib/K8s/cluster';
import { Box } from '@mui/material';
import { useEffect, useState } from 'react';
import { listHelmReleases, listHelmReleasesByNamespace } from '../api/helmReleases';
import { dynamicFilterFunction, Search } from './searchUtils';

export interface HelmReleaseListProps {
  resource?: KubeObject;
}

export default function HelmReleaseList(props: HelmReleaseListProps) {
  const { resource } = props;
  const namespace = resource?.jsonData?.metadata?.name ?? '';
  const [helmReleases, setHelmReleases] = useState<Array<any> | null>(null);
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    if (namespace === '') {
      listHelmReleases().then(response => {
        processResponse(response);
      });
    } else {
      listHelmReleasesByNamespace(namespace).then(response => {
        processResponse(response);
      });
    }
  }, [searchTerm]);

  function processResponse(response) {
    if (!response.items) {
      setHelmReleases([]);
      return;
    }
    const filterFunction = dynamicFilterFunction(searchTerm);
    const filteredResult = response.items.filter(filterFunction) ?? [];
    setHelmReleases(filteredResult);
    //console.log('Kustomizations: ', filteredResult);
  }

  return (
    <>
      <SectionHeader
        title="Flux Helm Releases"
        actions={[<Search searchTerm={searchTerm} setSearchTerm={setSearchTerm} />]}
      />
      <SimpleTable
        columns={[
          {
            label: 'Name',
            sort: true,
            getter: helmRelease => helmRelease.metadata?.name ?? '',
          },
          {
            label: 'Namespace',
            getter: helmRelease => (
              <Box display="flex" alignItems="center">
                <Box ml={1}>
                  <Link
                    routeName="namespace"
                    params={{
                      name: helmRelease.metadata.namespace,
                    }}
                  >
                    {helmRelease.metadata?.namespace ?? ''}
                  </Link>
                </Box>
              </Box>
            ),
          },
          {
            label: 'Chart',
            getter: helmRelease => helmRelease.spec?.chart?.spec?.chart ?? '',
            sort: true,
            gridTemplate: 1,
          },
          {
            label: 'Status',
            getter: helmChart => {
              // Get type of "Ready"
              const conditions = helmChart.status?.conditions ?? [];
              let message = '';
              let status = 'Unknown';
              conditions.forEach(condition => {
                if (condition.type === 'Ready') {
                  status = condition.status ?? 'Unknown';
                  message = condition.message ?? '';
                }
              });
              return (
                <StatusLabel status={status === 'True' ? 'success' : 'error'}>
                  {message}
                </StatusLabel>
              );
            },
            gridTemplate: 1.5,
          },
          {
            label: 'Last Applied',
            getter: helmRelease => helmRelease.status?.lastAppliedRevision ?? '',
            sort: true,
            gridTemplate: 0.5,
          },
          {
            label: 'Interval',
            getter: helmRelease => helmRelease.spec?.interval ?? '',
            sort: true,
            gridTemplate: 0.5,
          },
          {
            label: 'Created',
            getter: helmRelease => helmRelease.metadata?.creationTimestamp ?? '',
            sort: true,
          },
        ]}
        data={helmReleases}
      />
    </>
  );
}
