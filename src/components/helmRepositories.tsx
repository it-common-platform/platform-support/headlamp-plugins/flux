import {
  Link,
  SectionHeader,
  SimpleTable,
  StatusLabel,
} from '@kinvolk/headlamp-plugin/lib/CommonComponents';
import { KubeObject } from '@kinvolk/headlamp-plugin/lib/K8s/cluster';
import { Box } from '@mui/material';
import { useEffect, useState } from 'react';
import { listHelmRepositories, listHelmRepositoriesByNamespace } from '../api/helmRepositories';
import { dynamicFilterFunction, Search } from './searchUtils';

export interface HelmRepositoryListProps {
  resource?: KubeObject;
}

export default function HelmRepositoryList(props: HelmRepositoryListProps) {
  const { resource } = props;
  const namespace = resource?.jsonData?.metadata?.name ?? '';
  const [helmRepositories, setHelmRepositories] = useState<Array<any> | null>(null);
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    if (namespace === '') {
      listHelmRepositories().then(response => {
        processResponse(response);
      });
    } else {
      listHelmRepositoriesByNamespace(namespace).then(response => {
        processResponse(response);
      });
    }
  }, [searchTerm]);

  function processResponse(response) {
    if (!response.items) {
      setHelmRepositories([]);
      return;
    }
    const filterFunction = dynamicFilterFunction(searchTerm);
    const filteredResult = response.items.filter(filterFunction) ?? [];
    setHelmRepositories(filteredResult);
    //console.log('Kustomizations: ', filteredResult);
  }

  return (
    <>
      <SectionHeader
        title="Flux Helm Repositories"
        actions={[<Search searchTerm={searchTerm} setSearchTerm={setSearchTerm} />]}
      />
      <SimpleTable
        columns={[
          {
            label: 'Name',
            sort: true,
            getter: helmRepository => helmRepository.metadata?.name ?? '',
            gridTemplate: 0.5,
          },
          {
            label: 'Namespace',
            getter: helmRepository => (
              <Box display="flex" alignItems="center">
                <Box ml={1}>
                  <Link
                    routeName="namespace"
                    params={{
                      name: helmRepository.metadata.namespace,
                    }}
                  >
                    {helmRepository.metadata?.namespace ?? ''}
                  </Link>
                </Box>
              </Box>
            ),
            gridTemplate: 0.5,
          },
          {
            label: 'URL',
            getter: helmRelease => helmRelease.spec?.url ?? '',
            sort: true,
            gridTemplate: 1,
          },
          {
            label: 'Status',
            getter: helmChart => {
              // Get type of "Ready"
              const conditions = helmChart.status?.conditions ?? [];
              4;
              let message = '';
              let status = 'Unknown';
              conditions.forEach(condition => {
                if (condition.type === 'Ready') {
                  status = condition.status ?? 'Unknown';
                  message = condition.message ?? '';
                }
              });
              return (
                <StatusLabel status={status === 'True' ? 'success' : 'error'}>
                  {message}
                </StatusLabel>
              );
            },
            gridTemplate: 2.5,
          },
          {
            label: 'Interval',
            getter: helmRepository => helmRepository.spec?.interval ?? '',
            sort: true,
            gridTemplate: 0.2,
          },
          {
            label: 'Created',
            getter: helmRepository => helmRepository.metadata?.creationTimestamp ?? '',
            sort: true,
          },
        ]}
        data={helmRepositories}
      />
    </>
  );
}
