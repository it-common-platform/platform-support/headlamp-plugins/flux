import {
  Link,
  SectionHeader,
  SimpleTable,
  StatusLabel,
} from '@kinvolk/headlamp-plugin/lib/CommonComponents';
import { KubeObject } from '@kinvolk/headlamp-plugin/lib/K8s/cluster';
import { Box } from '@mui/material';
import { useEffect, useState } from 'react';
import { listHelmCharts, listHelmChartsByNamespace } from '../api/helmCharts';
import { dynamicFilterFunction, Search } from './searchUtils';

export interface HelmChartListProps {
  resource?: KubeObject;
}

export default function HelmChartList(props: HelmChartListProps) {
  const { resource } = props;
  const namespace = resource?.jsonData?.metadata?.name ?? '';
  const [helmCharts, setHelmCharts] = useState<Array<any> | null>(null);
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    if (namespace === '') {
      listHelmCharts().then(response => {
        processResponse(response);
      });
    } else {
      listHelmChartsByNamespace(namespace).then(response => {
        processResponse(response);
      });
    }
  }, [searchTerm]);

  function processResponse(response: { items: any[]; }) {
    if (!response.items) {
      setHelmCharts([]);
      return;
    }
    const filterFunction = dynamicFilterFunction(searchTerm);
    const filteredResult = response.items.filter(filterFunction) ?? [];
    setHelmCharts(filteredResult);
    //console.log('Kustomizations: ', filteredResult);
  }

  return (
    <>
      <SectionHeader
        title="Flux Helm Charts"
        actions={[<Search searchTerm={searchTerm} setSearchTerm={setSearchTerm} />]}
      />
      <SimpleTable
        columns={[
          {
            label: 'Flux HelmChart Name',
            sort: true,
            getter: helmChart => helmChart.metadata?.name ?? '',
          },
          {
            label: 'Namespace',
            getter: helmChart => (
              <Box display="flex" alignItems="center">
                <Box ml={1}>
                  <Link
                    routeName="namespace"
                    params={{
                      name: helmChart.metadata.namespace,
                    }}
                  >
                    {helmChart.metadata?.namespace ?? ''}
                  </Link>
                </Box>
              </Box>
            ),
          },
          {
            label: 'Status',
            getter: helmChart => {
              // Get type of "Ready"
              const conditions = helmChart.status?.conditions ?? [];
              let message = '';
              let status = 'Unknown';
              conditions.forEach((condition: { type: string; status: string; message: string; }) => {
                if (condition.type === 'Ready') {
                  status = condition.status ?? 'Unknown';
                  message = condition.message ?? '';
                }
              });
              return (
                <StatusLabel status={status === 'True' ? 'success' : 'error'}>
                  {message}
                </StatusLabel>
              );
            },
            gridTemplate: 2.3,
          },
          {
            label: 'Reconcile Strategy',
            getter: helmChart => helmChart.spec?.reconcileStrategy ?? '',
            sort: true,
            gridTemplate: 0.5,
          },
          {
            label: 'Source',
            getter: helmChart => helmChart.spec?.sourceRef?.name ?? '',
            sort: true,
          },
          {
            label: 'Interval',
            getter: kustomization => kustomization.spec?.interval ?? '',
            sort: true,
            gridTemplate: 0.5,
          },
          {
            label: 'Created',
            getter: helmChart => helmChart.metadata?.creationTimestamp ?? '',
            sort: true,
          },
        ]}
        data={helmCharts}
      />
    </>
  );
}
