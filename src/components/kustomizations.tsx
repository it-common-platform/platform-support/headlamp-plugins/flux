import {
  Link,
  SectionHeader,
  SimpleTable,
  StatusLabel,
} from '@kinvolk/headlamp-plugin/lib/CommonComponents';
import { KubeObject } from '@kinvolk/headlamp-plugin/lib/K8s/cluster';
import { Box } from '@mui/material';
import { useEffect, useState } from 'react';
import { listKustomizations, listKustomizationsByNamespace } from '../api/kustomizations';
import { dynamicFilterFunction, Search } from './searchUtils';

export interface KustomizationListProps {
  resource?: KubeObject;
}

export default function KustomizationList(props: KustomizationListProps) {
  const { resource } = props;
  const namespace = resource?.jsonData?.metadata?.name ?? '';
  const [kustomizations, setKustomizations] = useState<Array<any> | null>(null);
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    if (namespace === '') {
      listKustomizations().then(response => {
        processResponse(response);
      });
    } else {
      listKustomizationsByNamespace(namespace).then(response => {
        processResponse(response);
      });
    }
  }, [searchTerm]);

  function processResponse(response) {
    if (!response.items) {
      setKustomizations([]);
      return;
    }
    const filterFunction = dynamicFilterFunction(searchTerm);
    const filteredResult = response.items.filter(filterFunction) ?? [];
    setKustomizations(filteredResult);
    //console.log('Kustomizations: ', filteredResult);
  }

  return (
    <>
      <SectionHeader
        title="Flux Kustomizations"
        actions={[<Search searchTerm={searchTerm} setSearchTerm={setSearchTerm} />]}
      />
      <SimpleTable
        columns={[
          {
            label: 'Name',
            sort: true,
            getter: kustomization => kustomization.metadata?.name ?? '',
          },
          {
            label: 'Namespace',
            getter: kustomization => (
              <Box display="flex" alignItems="center">
                <Box ml={1}>
                  <Link
                    routeName="namespace"
                    params={{
                      name: kustomization.metadata.namespace,
                    }}
                  >
                    {kustomization.metadata.namespace}
                  </Link>
                </Box>
              </Box>
            ),
          },
          {
            label: 'Status',
            getter: kustomization => {
              // Get type of "Ready"
              const conditions = kustomization.status?.conditions ?? [];
              let message = '';
              let status = 'Unknown';
              conditions.forEach(condition => {
                if (condition.type === 'Ready') {
                  status = condition.status ?? 'Unknown';
                  message = condition.message ?? '';
                }
              });
              return (
                <StatusLabel status={status === 'True' ? 'success' : 'error'}>
                  {message}
                </StatusLabel>
              );
            },
            gridTemplate: 2.3,
          },
          {
            label: 'Interval',
            getter: kustomization => kustomization.spec?.interval ?? '',
            sort: true,
            gridTemplate: 0.5,
          },
          {
            label: 'Created',
            getter: kustomization => kustomization.metadata?.creationTimestamp ?? '',
            sort: true,
            gridTemplate: 0.8,
          },
        ]}
        data={kustomizations}
      />
    </>
  );
}
