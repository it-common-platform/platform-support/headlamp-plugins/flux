import { ApiProxy } from '@kinvolk/headlamp-plugin/lib';

const request = ApiProxy.request;
export const getHeadlampAPIHeaders = () => ({
  'X-HEADLAMP_BACKEND-TOKEN': new URLSearchParams(window.location.search).get('backendToken'),
});

export function listHelmRepositories() {
  return request('/apis/source.toolkit.fluxcd.io/v1/helmrepositories', {
    method: 'GET',
    headers: { ...getHeadlampAPIHeaders() },
  });
}

export function listHelmRepositoriesByNamespace(namespace: string) {
  return request(
    `/apis/source.toolkit.fluxcd.io/v1/namespaces/${namespace}/helmrepositories`,
    {
      method: 'GET',
      headers: { ...getHeadlampAPIHeaders() },
    }
  );
}

export function getHelmRelease(namespace: string, helmRepositoryName: string) {
  return request(
    `/apis/source.toolkit.fluxcd.io/v1/namespaces/${namespace}/helmrepositories?name=${helmRepositoryName}`,
    {
      method: 'GET',
      headers: { ...getHeadlampAPIHeaders() },
    }
  );
}
