import { ApiProxy } from '@kinvolk/headlamp-plugin/lib';

const request = ApiProxy.request;
export const getHeadlampAPIHeaders = () => ({
  'X-HEADLAMP_BACKEND-TOKEN': new URLSearchParams(window.location.search).get('backendToken'),
});

export function listHelmCharts() {
  return request('/apis/source.toolkit.fluxcd.io/v1/helmcharts', {
    method: 'GET',
    headers: { ...getHeadlampAPIHeaders() },
  });
}

export function listHelmChartsByNamespace(namespace: string) {
  return request(`/apis/source.toolkit.fluxcd.io/v1/namespaces/${namespace}/helmcharts`, {
    method: 'GET',
    headers: { ...getHeadlampAPIHeaders() },
  });
}

export function getHelmChart(namespace: string, reportName: string) {
  return request(
    `/apis/source.toolkit.fluxcd.io/v1/namespaces/${namespace}/helmcharts?name=${reportName}`,
    {
      method: 'GET',
      headers: { ...getHeadlampAPIHeaders() },
    }
  );
}
