import { ApiProxy } from '@kinvolk/headlamp-plugin/lib';

const request = ApiProxy.request;
export const getHeadlampAPIHeaders = () => ({
  'X-HEADLAMP_BACKEND-TOKEN': new URLSearchParams(window.location.search).get('backendToken'),
});

export function listHelmReleases() {
  return request('/apis/helm.toolkit.fluxcd.io/v2/helmreleases', {
    method: 'GET',
    headers: { ...getHeadlampAPIHeaders() },
  });
}

export function listHelmReleasesByNamespace(namespace: string) {
  return request(`/apis/helm.toolkit.fluxcd.io/v2/namespaces/${namespace}/helmreleases`, {
    method: 'GET',
    headers: { ...getHeadlampAPIHeaders() },
  });
}

export function getHelmRelease(namespace: string, helmReleaseName: string) {
  return request(
    `/apis/helm.toolkit.fluxcd.io/v2/namespaces/${namespace}/helmreleases?name=${helmReleaseName}`,
    {
      method: 'GET',
      headers: { ...getHeadlampAPIHeaders() },
    }
  );
}
