import { ApiProxy } from '@kinvolk/headlamp-plugin/lib';

const request = ApiProxy.request;
export const getHeadlampAPIHeaders = () => ({
  'X-HEADLAMP_BACKEND-TOKEN': new URLSearchParams(window.location.search).get('backendToken'),
});

export function listKustomizations() {
  return request('/apis/kustomize.toolkit.fluxcd.io/v1/kustomizations', {
    method: 'GET',
    headers: { ...getHeadlampAPIHeaders() },
  });
}

export function listKustomizationsByNamespace(namespace: string) {
  return request(`/apis/kustomize.toolkit.fluxcd.io/v1/namespaces/${namespace}/kustomizations`, {
    method: 'GET',
    headers: { ...getHeadlampAPIHeaders() },
  });
}

export function getKustomization(namespace: string, reportName: string) {
  return request(
    `/apis/kustomize.toolkit.fluxcd.io/v1/namespaces/${namespace}/kustomizations?name=${reportName}`,
    {
      method: 'GET',
      headers: { ...getHeadlampAPIHeaders() },
    }
  );
}
