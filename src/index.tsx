import {
  DefaultDetailsViewSection,
  registerDetailsViewSectionsProcessor,
  registerRoute,
  registerSidebarEntry,
} from '@kinvolk/headlamp-plugin/lib';
import HelmChartList from './components/helmCharts';
import HelmReleaseList from './components/helmReleases';
import HelmRepositoryList from './components/helmRepositories';
import KustomizationList from './components/kustomizations';

// Main Flux sidebar item
registerSidebarEntry({
  name: 'Flux',
  url: '/flux/kustomizations',
  icon: 'mdi:source-repository-multiple',
  label: 'Flux',
});

// ***** Flux Kustomizations *****
registerSidebarEntry({
  name: 'Kustomizations',
  url: '/flux/kustomizations',
  icon: '',
  parent: 'Flux',
  label: 'Kustomizations',
});

registerRoute({
  path: '/flux/kustomizations',
  sidebar: 'Kustomizations',
  name: 'KustomizationList',
  exact: true,
  component: () => <KustomizationList />,
});

// ***** Flux HelmReleases *****
registerSidebarEntry({
  name: 'HelmReleases',
  url: '/flux/helmReleases',
  icon: '',
  parent: 'Flux',
  label: 'Helm Releases',
});

registerRoute({
  path: '/flux/helmReleases',
  sidebar: 'HelmReleases',
  name: 'HelmReleasessList',
  exact: true,
  component: () => <HelmReleaseList />,
});

// ***** Flux HelmCharts *****
registerSidebarEntry({
  name: 'HelmCharts',
  url: '/flux/helmCharts',
  icon: '',
  parent: 'Flux',
  label: 'Helm Charts',
});

registerRoute({
  path: '/flux/helmCharts',
  sidebar: 'HelmCharts',
  name: 'HelmChartList',
  exact: true,
  component: () => <HelmChartList />,
});

// ***** Flux HelmRepositories *****
registerSidebarEntry({
  name: 'HelmRepositories',
  url: '/flux/helmRepositories',
  icon: '',
  parent: 'Flux',
  label: 'Helm Repositories',
});

registerRoute({
  path: '/flux/helmRepositories',
  sidebar: 'HelmRepositories',
  name: 'HelmRepositoryList',
  exact: true,
  component: () => <HelmRepositoryList />,
});

registerDetailsViewSectionsProcessor(function addSubheaderSection(resource, sections) {
  // Ignore if there is no resource.
  if (!resource) {
    //console.log('No resource found');
    return sections;
  }

  //console.log('Resource kind is', resource.kind);
  //const namespace = resource.jsonData.metadata.name;
  //console.log('Namespace is:', namespace);
  //console.log('Resource is:', resource);

  if (resource.kind !== 'Namespace') {
    // Return early if we're not on a namespace page
    return sections;
  }

  // Check if we already have added our custom section (this function may be called multiple times).
  const customSectionId = 'add-flux-resources-to-namespace';
  if (sections.findIndex(section => section.id === customSectionId) !== -1) {
    return sections;
  }

  const detailsHeaderIdx = sections.findIndex(
    section => section.id === DefaultDetailsViewSection.MAIN_HEADER
  );
  // There is no header, so we do nothing.
  if (detailsHeaderIdx === -1) {
    return sections;
  }

  // We place our custom section after the header.
  sections.splice(detailsHeaderIdx + 4, 0, {
    id: customSectionId,
    section: (
      <>
        <KustomizationList resource={resource} />
        <HelmReleaseList resource={resource} />
        <HelmChartList resource={resource} />
        <HelmRepositoryList resource={resource} />
      </>
    ),
  });

  return sections;
});
